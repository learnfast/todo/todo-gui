import { render, screen, cleanup } from '@testing-library/react';
import App from './App';

beforeEach(() => {
  render(<App />);
});


test('Header Renders Correctly', () => {
  const headerElement = screen.getByRole("heading");
  expect(headerElement).toHaveTextContent("To Do List")
  const banner = screen.getByRole("banner");
  expect(banner).toHaveClass("App-header")
});


test('Logo Test', () => {
  const imgElement = screen.getByRole("img");
  expect(imgElement).toHaveClass("App-logo");
  expect(imgElement).toHaveAttribute('alt','logo');
});
