import React,{useState, useEffect} from "react";
import Axios from 'axios';
import logo from './logo.png';
import Form from './components/Form';
import List from './components/List';
import './App.css';


function App() {

  const [inputText, setInputText] = useState("");
  const [todos, setTodos] = useState([]);

  const getTodos = () => {
    Axios.get("http://apitodo.servisimyolda.net/list").then((response) => { 
    response.data?.forEach((item) => {
      console.log(item)
      let task = {
          text: item.task,
          id:  Math.random()*1000
      }

      setTodos(old => [...old, task]);
    })
  })};


useEffect(() => {
    getTodos();
}, [])


  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>To Do List</h1>

      <Form
        inputText={inputText}
        todos={todos}
        setTodos={setTodos}
        setInputText={setInputText}
      />
      <List setTodos={setTodos} todos={todos} />
      </header>
    </div>
  )
}

export default App;
