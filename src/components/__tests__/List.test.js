import { render, screen, cleanup} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import List from "../List";

beforeEach(() => {
    render(<List />);
});

test('To Do List Test', () => {
    const list_ul = screen.getByRole("list");
    expect(list_ul).toHaveClass("todo-list");
});