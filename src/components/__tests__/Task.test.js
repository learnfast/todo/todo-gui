import { render, screen, cleanup} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Task from "../Task";

beforeEach(() => {
    render(<Task />);
});

test('To Do List Test', () => {
    const list_div = screen.getByRole("listitem");
    expect(list_div).toBeInTheDocument();
  });