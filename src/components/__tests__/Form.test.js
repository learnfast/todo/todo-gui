import { render, screen, cleanup} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Form from "../Form";

beforeEach(() => {
    render(<Form />);
});

test('Form Input Test', () => {
    const inputBox = screen.getByRole("textbox");
    expect(inputBox).toBeInTheDocument();
    expect(inputBox).toHaveClass("todo-input");
    expect(inputBox.placeholder).toEqual('Write a Task!');
    expect(inputBox.type).toBe('text');
  });

  test('Form Button Test', () => {
    const addButton = screen.getByRole("button");
    expect(addButton).toBeInTheDocument();
    expect(addButton).toBeEnabled();
    expect(addButton).toHaveClass("todo-button");
    expect(addButton.type).toBe("submit");
  });

  test('Input Values Changing When Key Pressed',() =>{
    const todoinput = screen.getByRole("textbox");
    expect(todoinput).toHaveValue("");
    // userEvent.type(todoinput,"t");
    // expect(todoinput).toHaveValue("t");
  });
