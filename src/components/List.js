// import { render } from '@testing-library/react';
// import React,{ Component} from 'react'
import '../App.css';
import Task from "./Task"

const List = ({ todos, setTodos }) => {
    // console.log(todos);
        return(
            <div className="todo-container">
                <ul className="todo-list">
                    {todos?.map(todo => (
                        <Task 
                            setTodos={setTodos} 
                            todos={todos} 
                            key={todo.id}
                            todo={todo}
                            text={todo.text}
                        />
                    ))}
                </ul>
            </div>
        );
    };

export default List;