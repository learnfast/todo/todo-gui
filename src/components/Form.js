// import React from 'react';
import React from "react";
import Axios from 'axios';


const Form = ({ setInputText,todos,setTodos, inputText }) => {

    const postTodos = (task) => {
        Axios.post("http://apitodo.servisimyolda.net/task", {"task":task}).then((res) => {console.log(res.data);});
      }



    const inputTextHandler = (e) => {
        console.log(e.target.value);
        setInputText(e.target.value);
    };
    const submitTodoHandler = (e) => {
            e.preventDefault();
            postTodos(inputText);
            setTodos([
                    ...todos,
                    {text: inputText, id: Math.random()*1000 },
                ])
            setInputText('');

        };


    return(
        <form>
            <input
                value={inputText}
                onChange={inputTextHandler}
                type="text"
                placeholder="Write a Task!"
                className="todo-input"
            />
            <button onClick={submitTodoHandler} className="todo-button" type="submit">
                <i className="fas fa-plus-square"></i>
            </button>
        </form>
    );
};


export default Form;
