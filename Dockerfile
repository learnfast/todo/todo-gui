FROM node
WORKDIR /app
COPY package.json .
RUN npm install
RUN npm install -g serve
COPY . .
EXPOSE 3000
RUN npm run build
CMD ["npx","serve","-s","build","-p","3000"]